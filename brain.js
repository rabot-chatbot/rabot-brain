const BrainJSClassifier = require('natural-brain');

/**
 * Default way of dealing with errors in this module
 * @param error
 */
const onErrorDefault = (error) => console.error(error);

/**
 * BRAIN: Classify utterance to intent
 * @param text
 * @param ACCURACY_BARRIER
 * @param callback
 * @param mismatchCallback
 * @param onError
 */
const getIntent = (text, ACCURACY_BARRIER, callback, mismatchCallback, onError = onErrorDefault) => {
    BrainJSClassifier.load('./classifier/classifier.json', null, null, (error, classifier) => {
        if (error) {
            onError();
        } else {
            const guessedLabel = classifier.classify(text);
            const matchList = classifier.getClassifications(text).map(formatData).sort((a, b) => b.accuracy - a.accuracy);
            const matchedAction = matchList.find(({label}) => label === guessedLabel);

            // Decide if guessed label is accurate enough
            if (matchedAction.accuracy > ACCURACY_BARRIER) {
                callback(matchedAction);
            } else {
                mismatchCallback({matchList});
            }
        }
    });
};

/**
 * Format data from Natural to a more readable object
 * @returns {{app: *, intent: *}}
 * @param label
 * @param value
 */
const formatData = ({label, value}) => {
    const [app, intent] = label.split('.');

    return {
        label,
        app,
        intent,
        accuracy: value
    }
};

module.exports = getIntent;